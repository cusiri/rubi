Git Work Flow
-When you start work, move the card in trello into the in progress tab from the current sprint the completed when done
-Always pull from master before you start work/ open your project so your project is up to date.
- When making changes create a local branch git checkout -b "branch name" and a branch on bitbucket. When you are finished and have tested your changes push to your branch and make a pull request to master

This is a git cheat sheet for quick set up 
http://kbroman.org/github_tutorial/pages/init.html

This is a git cheat sheet with more comprehensive commands
https://www.git-tower.com/blog/git-cheat-sheet/
