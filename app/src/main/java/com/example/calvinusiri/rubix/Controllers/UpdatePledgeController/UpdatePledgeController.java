package com.example.calvinusiri.rubix.Controllers.UpdatePledgeController;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.HideKeyBoard;

public class UpdatePledgeController extends AppCompatActivity {
    private Button finishButton;
    private EditText updatePaidAmountPledgeEditText;
    private RelativeLayout mainLayout;
    private String globalCampaignId;
    private String globalPledgeId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_pledges);
        finishButton = findViewById(R.id.button_finish_update_pledge_controller_id);
        updatePaidAmountPledgeEditText = findViewById(R.id.edit_text_payment_amount_update_pledge_controller_id);
        mainLayout = findViewById(R.id.main_layout_update_pledges_controller);
        HideKeyBoard hideKeyBoard = new HideKeyBoard(UpdatePledgeController.this);
        hideKeyBoard.setupUI(mainLayout);
        setUpFinishButton(finishButton);
        getIncomingIntents();
    }

    private void getIncomingIntents(){
        if (getIntent().hasExtra("pledge_campaign_id")&& getIntent().hasExtra("pledge_id")) {
            globalCampaignId = getIntent().getStringExtra("pledge_campaign_id");
            globalPledgeId = getIntent().getStringExtra("pledge_id");
        }
    }
    //On Click Listeners
    private void setUpFinishButton(Button finishButton){
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String paidAmount = updatePaidAmountPledgeEditText.getText().toString().trim();
                    if(!TextUtils.isEmpty(paidAmount)){
                        //Update amount paid in campaign total
                        FirebaseConnectors.updateTotalAmountFundedInCampaign(paidAmount,globalCampaignId);
                        // Update Amount paid within pledges
                        FirebaseConnectors.updateAmountPaidFromAnIndividualPledge(paidAmount,globalCampaignId,globalPledgeId);
                        finish();
                    }else{
                        finish();
                    }
                }catch (Exception e){
                    Log.d("UpdatePledgeController",e.toString());
                }
            }
        });
    }
}
