package com.example.calvinusiri.rubix.Controllers.FeedFragmentController;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.calvinusiri.rubix.Controllers.ViewCampaignProfileActivityController.CampaignProfileViewController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.model.Campaign;

import java.util.ArrayList;

public class FeedFragmentRecyclerViewAdapter extends RecyclerView.Adapter<FeedFragmentRecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "RecyclerViewAdapter";
    // Holds Image Names
    private Context mContext;
    private ArrayList<Campaign> campaignArray;

    public FeedFragmentRecyclerViewAdapter(Context mContext, ArrayList<Campaign> campaignArray) {
        Log.d(TAG, "RecyclerViewAdapter: Called.");
        this.campaignArray = campaignArray;
        this.mContext = mContext;
    }
    //This method is responsible for creating the view
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feed_card_view,parent,false);
        //Create a view holder object, which is the class that we created
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
    // This is called every time a new item is added to the list
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        FirebaseConnectors.getSingleUserFromDB(campaignArray.get(position).getOrganizerId(),holder.organizerCampaignNameTextView,holder.organizerPhoneNumberTextView);
        FirebaseConnectors.initGetImageUriCampaign(campaignArray.get(position),holder.image,mContext,holder.campaignImageProgressBar);

        float percentageFunded = (Float.parseFloat(campaignArray.get(position).getAmountFunded())/Float.parseFloat(campaignArray.get(position).getTargetAmount()))*100;
        final int valuePercentageFunded = (int) percentageFunded;
        holder.title.setText(campaignArray.get(position).getTitle());
        holder.description.setText(campaignArray.get(position).getDescription());
        holder.targetAmount.setText(campaignArray.get(position).getTargetAmount());
        holder.funded.setText(Integer.toString(valuePercentageFunded)+"%");
        holder.progressBar.setProgress(Integer.parseInt(Integer.toString(valuePercentageFunded)));
        holder.pledgedAmount.setText(campaignArray.get(position).getTotalPledged());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CampaignProfileViewController.class);
                try{
                    intent.putExtra("downloaded_image_url",campaignArray.get(position).getDownloadedCampaignUrl().toString());
                    intent.putExtra("campaign_id",campaignArray.get(position).getCampaignId());
                    intent.putExtra("campaign_title",campaignArray.get(position).getTitle());
                    intent.putExtra("campaign_organizer_id",campaignArray.get(position).getOrganizerId());
                    intent.putExtra("campaign_funded",Integer.toString(valuePercentageFunded)+"%");
                    intent.putExtra("campaign_backers",campaignArray.get(position).getTotalPledged());
                    intent.putExtra("campaign_progress",Integer.toString(valuePercentageFunded));
                    intent.putExtra("campaign_target_amount",campaignArray.get(position).getTargetAmount());
                    intent.putExtra("campaign_pledge_amount",campaignArray.get(position).getTotalPledged());
                    intent.putExtra("campaign_description",campaignArray.get(position).getDescription());
                }catch (Exception exception){
                    System.out.println(exception);
                }
                mContext.startActivity(intent);// Need to reference context because we are in RecyclerView Class
            }
        });
        setAnimation(holder.parentLayout,position);
    }
    @Override
    public int getItemCount() {
        return campaignArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        private TextView title;
        private TextView description;
        private ProgressBar progressBar;
        private TextView targetAmount;
        private TextView funded;
        private ProgressBar campaignImageProgressBar;
        private TextView pledgedAmount;
        private TextView organizerPhoneNumberTextView;
        private TextView organizerCampaignNameTextView;
        CardView parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.card_view_image_id);
            title = itemView.findViewById(R.id.text_view_title);
            description = itemView.findViewById(R.id.text_view_description);
            progressBar = itemView.findViewById(R.id.feed_progress);
            targetAmount = itemView.findViewById(R.id.text_view_target_id);
            funded = itemView.findViewById(R.id.text_view_amount_funded_id);
            campaignImageProgressBar = itemView.findViewById(R.id.campaign_image_progress_bar);
            pledgedAmount = itemView.findViewById(R.id.text_view_pledged_id);
            organizerPhoneNumberTextView = itemView.findViewById(R.id.text_view_display_phone_number);
            organizerCampaignNameTextView = itemView.findViewById(R.id.text_view_display_organizer_name);
            parentLayout = itemView.findViewById(R.id.feed_card_view_id);
        }
    }// Holds the widgets in memory
    private void setAnimation(View viewToAnimate, int position)
    {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_left_in);
        viewToAnimate.startAnimation(animation);
    }
}
