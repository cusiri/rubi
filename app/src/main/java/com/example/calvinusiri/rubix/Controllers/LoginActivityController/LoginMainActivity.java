package com.example.calvinusiri.rubix.Controllers.LoginActivityController;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calvinusiri.rubix.Controllers.FeedActivityController.FeedMainActivity;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.HideKeyBoard;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class LoginMainActivity extends AppCompatActivity{
    private static final String TAG = "LoginMainActivity";
    private EditText phoneNumberEditText,passwordEditText;
    private TextView passwordTextView;
    private EditText displayNameEditText;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private Button loginButton;
    private RelativeLayout mainLayout;
    private Context mContext = LoginMainActivity.this;
    private String mVerificationId;
    private int buttonType=0;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: CREATED");
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() != null){
            Log.d(TAG, "onStart: Logged IN");
            performSeque(mContext,FeedMainActivity.class);
        }
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: Not Logged In");
        setContentView(R.layout.activity_login_main);
        mainLayout = findViewById(R.id.main_login_layout);
        phoneNumberEditText = findViewById(R.id.login_phone_number_edit_text_id);
        passwordEditText = findViewById(R.id.login_password_edit_text_id);
        passwordTextView = findViewById(R.id.login_password_text_view_id);
        displayNameEditText = findViewById(R.id.login_display_name_edit_text_id);
        progressBar = findViewById(R.id.progressBar);
        loginButton = findViewById(R.id.login_button_id);
        progressBar.setVisibility(View.INVISIBLE);
        HideKeyBoard hideKeyBoard = new HideKeyBoard(LoginMainActivity.this);
        hideKeyBoard.setupUI(mainLayout);
        onClickLoginButton(loginButton);
    }

    // Segue to signup activity
    public void performSeque(Context context, Class view){
        Intent intent = new Intent(context, view);
        startActivity(intent);
        finish();
    }
    private void onClickLoginButton(Button loginButton){
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonType == 0){
                    //Code has not been sent, sent code
                    final String phoneNumber = phoneNumberEditText.getText().toString().trim();
                    if(!TextUtils.isEmpty(phoneNumber)){
                        if(phoneNumber.charAt(0)=='0'){
                            StringBuilder sb = new StringBuilder(phoneNumber);
                            sb.deleteCharAt(0);
                            loginUser("+255"+sb.toString());
                        }else{
                            loginUser(phoneNumber);
                        }
                    }
                }
                else {
                    //Start Verification process after code has been sent
                    String verificationCode = passwordEditText.getText().toString().trim();
                    if(!TextUtils.isEmpty(verificationCode)){
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId,verificationCode);
                        progressBar.setVisibility(View.VISIBLE);
                        signInWithPhoneAuthCredential(credential);
                    }
                }
            }
        });
    }
    public void loginUser(String phoneNumber){
        Toast.makeText(LoginMainActivity.this,"We are sending you a verification code, standard rates may apply.",Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.VISIBLE);
        phoneNumberEditText.setEnabled(false);
        loginButton.setEnabled(false);
        auth.useAppLanguage();// Apply to default app language
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        // This callback will be invoked in two situations:
                        // 1 - Instant verification. In some cases the phone number can be instantly
                        //     verified without needing to send or enter a verification code.
                        // 2 - Auto-retrieval. On some devices Google Play services can automatically
                        //     detect the incoming verification SMS and perform verification without
                        //     user action.
                        Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                    }
                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        // This callback is invoked in an invalid request for verification is made,
                        // for instance if the the phone number format is not valid.
                        Log.w(TAG, "onVerificationFailed", e);

                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            // Invalid request
                            Toast.makeText(LoginMainActivity.this,"Please check your phone number again",Toast.LENGTH_LONG).show();
                            phoneNumberEditText.setEnabled(true);
                            loginButton.setEnabled(true);
                            progressBar.setVisibility(View.GONE);
                            // ...
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            // The SMS quota for the project has been exceeded
                            Toast.makeText(LoginMainActivity.this,"Please try again in 10 minutes",Toast.LENGTH_LONG).show();
                        }
                        // Show a message and update the UI
                    }

                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken token) {
                        // The SMS verification code has been sent to the provided phone number, we
                        // now need to ask the user to enter the code and then construct a credential
                        // by combining the code with a verification ID.
                        Log.d(TAG, "onCodeSent:" + verificationId);
                        // Save verification ID and resending token so we can use them later
                        mVerificationId = verificationId;
                        mResendToken = token;
                        passwordEditText.setVisibility(View.VISIBLE);
                        passwordTextView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        loginButton.setEnabled(true);
                        buttonType = 1;
                    }
                });        // OnVerificationStateChangedCallbacks
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = task.getResult().getUser();
                            final String displayName = displayNameEditText.getText().toString();
                            FirebaseConnectors.launchUserInformationToDatabase(getString(R.string.firebase_users_child),displayName,user.getPhoneNumber(),user.getUid());
                            performSeque(LoginMainActivity.this,FeedMainActivity.class);
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(LoginMainActivity.this,"Verification Code Entered Was Incorrect, Resend Code",Toast.LENGTH_LONG).show();
                                buttonType = 0;
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }
}

