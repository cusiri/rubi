package com.example.calvinusiri.rubix.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    private String displayName;
    private String phoneNumber;

    public User(){}
    public User(String phoneNumber,String displayName) {
        this.phoneNumber = phoneNumber;
        this.displayName = displayName;
    }
    public String getDisplayName() {
        return displayName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
}
