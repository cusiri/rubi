package com.example.calvinusiri.rubix.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentsSectionsStatePagerAdapter extends FragmentStatePagerAdapter {
    //Keeping track of fragments
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final HashMap<Fragment,Integer> mFragements = new HashMap<>();
    private final HashMap<String,Integer> mFragementsNumbers = new HashMap<>();
    private final HashMap<Integer,String> mFragementsName = new HashMap<>();

    public FragmentsSectionsStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    // Used to add fragments to adapter
    public void addFragment(Fragment fragment){
        mFragmentList.add(fragment);
    }
}
