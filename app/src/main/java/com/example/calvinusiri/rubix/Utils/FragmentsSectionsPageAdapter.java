package com.example.calvinusiri.rubix.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentsSectionsPageAdapter extends FragmentPagerAdapter{
    //Keeping track of fragments
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    //Keeping Track of The Name Of The Fragment
    public FragmentsSectionsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position); // Need to return this and not null or else well get null pointer
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
    // returns the title of the fragment
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    // Used to add fragments to adapter
    public void addFragment(Fragment fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }
}
