package com.example.calvinusiri.rubix.Controllers.ViewPledgesFragementController;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.calvinusiri.rubix.Controllers.ManagePledgeActivityController.ManagePledgeActivityController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.model.Pledges;

import java.util.ArrayList;

public class PledgeFragmentRecyclerViewAdapter extends RecyclerView.Adapter<PledgeFragmentRecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "PledgeFragmentRecyclerV";
    private Context mContext;
    private String campaignId;
    private ArrayList<Pledges> pledgesArrayList;

    public PledgeFragmentRecyclerViewAdapter(Context mContext, ArrayList<Pledges> pledgesArrayList, String campaignId) {
        this.mContext = mContext;
        this.pledgesArrayList = pledgesArrayList;
        this.campaignId = campaignId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_view_pledges_view, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String name = pledgesArrayList.get(position).getName();
        Character firstLetter = name.charAt(0);
        int size = name.length()-1;
        String lastLetter = name.substring(size);
        System.out.println(firstLetter);
        System.out.println("Name"+name);
        System.out.println(name.charAt(size));
        System.out.println(Character.toString(firstLetter)+lastLetter);
        holder.idImageTextView.setText(Character.toString(firstLetter)+lastLetter);
        setUpStatusUi(pledgesArrayList.get(position).getPledgeAmount(),pledgesArrayList.get(position).getPaid(),holder.paid,holder.idImageTextView);
        holder.comment.setText(pledgesArrayList.get(position).getComment());
        holder.displayNameTextView.setText(name);
        holder.pledgeAmount.setText("Pledge Amount: "+ pledgesArrayList.get(position).getPledgeAmount());
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent intent = new Intent(mContext, ManagePledgeActivityController.class);
                    intent.putExtra("pledge_name",pledgesArrayList.get(position).getName());
                    intent.putExtra("pledge_amount",pledgesArrayList.get(position).getPledgeAmount());
                    intent.putExtra("pledge_paid",pledgesArrayList.get(position).getPaid());
                    intent.putExtra("pledge_campaign_id",campaignId);
                    intent.putExtra("pledge_id",pledgesArrayList.get(position).getId());
                    intent.putExtra("pledge_phonenumber",pledgesArrayList.get(position).getPhoneNumber());
                    mContext.startActivity(intent);// Need to reference context because we are in RecyclerView Class
                }catch (Exception e){
                    Log.d(TAG,e.toString());
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return pledgesArrayList.size();
    }

    //View Holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView idImageTextView;
        private TextView displayNameTextView;
        private TextView paid;
        private TextView comment;
        private TextView pledgeAmount;
        private CardView mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            idImageTextView = itemView.findViewById(R.id.pledges_text_view_image_name);
            displayNameTextView = itemView.findViewById(R.id.pledges_text_view_display_name);
            paid = itemView.findViewById(R.id.pledges_text_view_paid_label);
            comment = itemView.findViewById(R.id.pledges_text_view_comment);
            pledgeAmount = itemView.findViewById(R.id.pledges_text_view_pledge_amount);
            mainLayout = itemView.findViewById(R.id.feed_card_view_id);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUpStatusUi(String amount, String amountPaid, TextView paid, TextView imageNameIcon){
        if(Float.parseFloat(amountPaid)==0.0){
            paid.setText(mContext.getText(R.string.notPaid));
            paid.setTextColor(mContext.getColor(R.color.red));
        }
        if(Float.parseFloat(amountPaid) >= Integer.parseInt(amount)){
            paid.setText(mContext.getText(R.string.paid));
            paid.setTextColor(mContext.getColor(R.color.gradStart));
        }
    }
}
