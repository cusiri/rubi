package com.example.calvinusiri.rubix.model;

import android.net.Uri;

public class Campaign {
    private static final String TAG = "Campaign";
    private String campaignId;
    private String organizerId;
    private String title;
    private String description;
    private String targetAmount;
    private String totalPledged;
    private String amountFunded;
    private String imageCampaignUrl;
    private Uri downloadedCampaignUrl;
    private String phoneNumbers;

    public Campaign() {}
    public Campaign(String organizerId, String title, String description, String targetAmount, String totalPledged, String amoundFunded, String imageCampaignUrl, String phoneNumber) {
        this.organizerId = organizerId;
        this.title = title;
        this.description = description;
        this.targetAmount = targetAmount;
        this.totalPledged = totalPledged;
        this.amountFunded = amoundFunded;
        this.imageCampaignUrl = imageCampaignUrl;
        this.phoneNumbers = phoneNumber;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTargetAmount() {
        return targetAmount;
    }

    public String getTotalPledged() {
        return totalPledged;
    }

    public void setTotalPledged(String totalPledged) {
        this.totalPledged = totalPledged;
    }

    public String getAmountFunded() {
        return amountFunded;
    }

    public void setAmountFunded(String amountFunded) {
        this.amountFunded = amountFunded;
    }

    public String getImageCampaignUrl() {
        return imageCampaignUrl;
    }

    public void setDownloadedCampaignUrl(Uri downloadedCampaignUrl) {
        this.downloadedCampaignUrl = downloadedCampaignUrl;
    }
    public Uri getDownloadedCampaignUrl() {
        return downloadedCampaignUrl;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }
}

