package com.example.calvinusiri.rubix.Controllers.SearchFragmentController;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.calvinusiri.rubix.Controllers.ViewCampaignProfileActivityController.CampaignProfileViewController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.model.Campaign;

import java.util.ArrayList;

public class SearchFragmentRecyclerViewAdapter extends RecyclerView.Adapter<SearchFragmentRecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "SearchFragmentRecyclerV";
    private ArrayList<Campaign> mSearchedCampaign;
    public static Handler searchFragementMHandler;
    private Context mContext;

    public SearchFragmentRecyclerViewAdapter(ArrayList<Campaign> mSearchedCampaign, Context mContext) {
        this.mSearchedCampaign = mSearchedCampaign;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_fragment_card_view,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        FirebaseConnectors.initGetImageUriCampaign(mSearchedCampaign.get(position),holder.campaignImage,mContext,holder.progressBar);
        holder.campaignTitle.setText(mSearchedCampaign.get(position).getTitle());
        holder.progressBar.setVisibility(View.VISIBLE);
        float percentageFunded = (Float.parseFloat(mSearchedCampaign.get(position).getAmountFunded())/Float.parseFloat(mSearchedCampaign.get(position).getTargetAmount()))*100;
        final int valuePercentageFunded = (int) percentageFunded;
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CampaignProfileViewController.class);
                try{
                    intent.putExtra("downloaded_image_url",mSearchedCampaign.get(position).getDownloadedCampaignUrl().toString());
                    intent.putExtra("campaign_id",mSearchedCampaign.get(position).getCampaignId());
                    intent.putExtra("campaign_title",mSearchedCampaign.get(position).getTitle());
                    intent.putExtra("campaign_organizer_id",mSearchedCampaign.get(position).getOrganizerId());
                    intent.putExtra("campaign_funded",Integer.toString(valuePercentageFunded)+"%");
                    intent.putExtra("campaign_backers",mSearchedCampaign.get(position).getTotalPledged());
                    intent.putExtra("campaign_progress",Integer.toString(valuePercentageFunded));
                    intent.putExtra("campaign_target_amount",mSearchedCampaign.get(position).getTargetAmount());
                    intent.putExtra("campaign_pledge_amount",mSearchedCampaign.get(position).getTotalPledged());
                    intent.putExtra("campaign_description",mSearchedCampaign.get(position).getDescription());
                }catch (Exception exception){
                    System.out.println(exception);
                }
                mContext.startActivity(intent);// Need to reference context because we are in RecyclerView Class
            }
        });
        setAnimation(holder.parentLayout,position);
    }

    @Override
    public int getItemCount() {
        return mSearchedCampaign.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView campaignTitle;
        private ImageView campaignImage;
        private ProgressBar progressBar;
        private CardView parentLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            campaignImage = itemView.findViewById(R.id.card_view_image_id);
            progressBar = itemView.findViewById(R.id.campaign_image_progress_bar);
            campaignTitle = itemView.findViewById(R.id.text_view_title);
            parentLayout = itemView.findViewById(R.id.fragment_search_view_card_view);
        }
    }
    private void setAnimation(View viewToAnimate, int position)
    {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.push_left_in);
        viewToAnimate.startAnimation(animation);
    }
}
