package com.example.calvinusiri.rubix.Controllers.ViewPledgesFragementController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;

import java.util.ArrayList;

public class ViewPledgesFragmentController extends Fragment {
    private ArrayList pledgesArrayList;
    private static final String TAG = "ViewPledgesFragmentCont";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pledges,container, false);
        final RecyclerView recyclerView = view.findViewById(R.id.reusable_recycler_view);
        pledgesArrayList = new ArrayList();
        final PledgeFragmentRecyclerViewAdapter adapter = new PledgeFragmentRecyclerViewAdapter(getContext(),pledgesArrayList,getCampaignIdFromBundleId());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    FirebaseConnectors.getCampaignPledgesFromDB(pledgesArrayList,getCampaignIdFromBundleId(),adapter);
                }catch (Exception e){
                    Log.d(TAG,e.toString());
                }
            }
        });
        return view;
    }
    // This function returns the campaign id
    private String getCampaignIdFromBundleId(){
        return getArguments().getString("id");
    }
}
