package com.example.calvinusiri.rubix.model;

public class Pledges {
    private String id;
    private String name;
    private String comment;
    private String pledgeAmount;
    private String phoneNumber;
    private String paid;

    public Pledges(){}
    public Pledges(String name, String comment, String pledgeAmount,String phoneNumber,String paid) {
        this.name = name;
        this.comment = comment;
        this.pledgeAmount = pledgeAmount;
        this.phoneNumber = phoneNumber;
        this.paid = paid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPledgeAmount() {
        return pledgeAmount;
    }

    public void setPledgeAmount(String pledgeAmount) {
        this.pledgeAmount = pledgeAmount;
    }

}
