package com.example.calvinusiri.rubix.Controllers.ManagePledgeActivityController;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calvinusiri.rubix.Controllers.LoginActivityController.LoginMainActivity;
import com.example.calvinusiri.rubix.Controllers.UpdatePledgeController.UpdatePledgeController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.google.firebase.auth.FirebaseAuth;

public class ManagePledgeActivityController extends AppCompatActivity {
    private Button finishButton;
    private Button managePledges;
    private Button sendReminder;
    private Button deletePledge;
    private TextView pledgeAmountTextView;
    private TextView pledgeNameTextView;
    private TextView pledgeOutstandingTextView;
    private TextView statusOfPayment;
    private Context mContext = ManagePledgeActivityController.this;
    private FirebaseAuth auth;

    @Override
    protected void onStart() {
        super.onStart();
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() == null){
            performSeque(mContext,LoginMainActivity.class);
        }
    }
    // Segue to signup activity
    public void performSeque(Context context, Class view){
        Intent intent = new Intent(context, view);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_pledge);
        finishButton = findViewById(R.id.button_finish);
        managePledges = findViewById(R.id.button_manage_pledges);
        deletePledge = findViewById(R.id.manage_pledge_activity_controller_delete_pledge_id);
        sendReminder = findViewById(R.id.manage_pledge_activity_controller_send_reminder_button);
        pledgeAmountTextView  = findViewById(R.id.pledges_text_view_pledge_amount);
        pledgeNameTextView = findViewById(R.id.pledges_text_view_display_name);
        pledgeOutstandingTextView = findViewById(R.id.text_view_balance_id);
        statusOfPayment = findViewById(R.id.pledges_text_view_paid_label);
        getIncomingIntents();
        setUpOnClickListenerFinishedButton(finishButton);
    }

    private void getIncomingIntents(){
        if (getIntent().hasExtra("pledge_name") && getIntent().hasExtra("pledge_amount") &&
                getIntent().hasExtra("pledge_paid")&& getIntent().hasExtra("pledge_campaign_id")&& getIntent().hasExtra("pledge_id")
                && getIntent().hasExtra("pledge_phonenumber") ) {
            String pledgerName = getIntent().getStringExtra("pledge_name");
            String amount = getIntent().getStringExtra("pledge_amount");
            String paid = getIntent().getStringExtra("pledge_paid");
            String pledge_campaign_id = getIntent().getStringExtra("pledge_campaign_id");
            String pledge_id = getIntent().getStringExtra("pledge_id");
            String pledge_phonenumber = getIntent().getStringExtra("pledge_phonenumber");
            String balance = managePledgeBalance(amount,paid);
            setUpPledgesView(pledgerName,amount,paid,balance);
            setUpOnClickListenerManagePledges(managePledges,pledge_campaign_id,pledge_id);
            setUpOnClickListenerSendReminder(sendReminder,pledge_phonenumber,messge(pledgerName,balance));
            setUpOnClickListenerDeletePledge(deletePledge,pledge_campaign_id,pledge_id);
        }
    }
    // Set up view
    private void setUpPledgesView(String name,String amount,String paid,String balance){
        pledgeNameTextView.setText(name);
        pledgeAmountTextView.setText(amount);
        pledgeOutstandingTextView.setText(balance);
        if(Float.parseFloat(paid)==0.0){
            statusOfPayment.setText(getString(R.string.notPaid));
            statusOfPayment.setTextColor(getResources().getColor(R.color.red));
        }
        if(Float.parseFloat(paid) >= Integer.parseInt(amount)){
            statusOfPayment.setText(getString(R.string.paid));
            statusOfPayment.setTextColor(getResources().getColor(R.color.gradStart));
        }
    }
    //On Click Listerners
    private void setUpOnClickListenerFinishedButton(Button finishedButton){
        finishedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void setUpOnClickListenerManagePledges(Button managePledges, final String campaignId, final String pledge_id){
        managePledges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManagePledgeActivityController.this,UpdatePledgeController.class);
                intent.putExtra("pledge_campaign_id",campaignId);
                intent.putExtra("pledge_id",pledge_id);
                startActivity(intent);
                finish();
            }
        });
    }
    private void setUpOnClickListenerSendReminder(Button sendReminder, final String phoneNumber, final String message){
        sendReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(mContext, Manifest.permission.SEND_SMS) != getPackageManager().PERMISSION_GRANTED){
                    if(ActivityCompat.shouldShowRequestPermissionRationale(ManagePledgeActivityController.this,Manifest.permission.SEND_SMS)){
                        ActivityCompat.requestPermissions(ManagePledgeActivityController.this,new String[]{Manifest.permission.SEND_SMS},1);
                    }else{
                        ActivityCompat.requestPermissions(ManagePledgeActivityController.this,new String[]{Manifest.permission.SEND_SMS},1);
                    }
                }else{
                    try{
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phoneNumber,"rubi",message,null,null);
                        Toast.makeText(mContext,"Sent",Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        Toast.makeText(mContext,e.toString(),Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    private void setUpOnClickListenerDeletePledge(Button deletePledge, final String campaignId, final String pledgeId){
        deletePledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseConnectors.deletePlegeFromDB(campaignId,pledgeId,mContext,ManagePledgeActivityController.this);
            }
        });
    }
    //Return requests functions
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.SEND_SMS) == getPackageManager().PERMISSION_GRANTED){
                        Toast.makeText(mContext, "Permission Granted", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(mContext, "Permission Not Granted", Toast.LENGTH_SHORT).show();
                    }
                }
                return;
        }
    }
    //Util functions
    private String managePledgeBalance(String pledgeAmount, String paid){
        Float pledge = Float.parseFloat(pledgeAmount);
        Float paidAmount = Float.parseFloat(paid);
        return "Outstanding: "+String.valueOf(paidAmount-pledge);
    }
    private String messge(String name, String balance){
        String txt = "Hello "+name+"\n"+"Here is the information about the pledge you made. \n Your Balance: "+balance;
        return txt;
    }
}
