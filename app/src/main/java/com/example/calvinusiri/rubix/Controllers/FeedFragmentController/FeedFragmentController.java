package com.example.calvinusiri.rubix.Controllers.FeedFragmentController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.model.Campaign;

import java.util.ArrayList;

public class FeedFragmentController extends Fragment{
    private static final String TAG = "FeedFragmentController";
    private ArrayList<Campaign> campaigns;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_view, container, false);
        final RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        campaigns = new ArrayList<>();
        final FeedFragmentRecyclerViewAdapter adapter = new FeedFragmentRecyclerViewAdapter(getContext(),campaigns);//This is our default constructor that we made in our adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FirebaseConnectors.initGetRawtextData(campaigns,adapter);
            }
        });
        return view;
    }
}
