package com.example.calvinusiri.rubix.Controllers.CreatePledgeFragmentController;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.HideKeyBoard;
import com.example.calvinusiri.rubix.model.User;

public class CreatePledgeFragementController extends Fragment {
    private static final String TAG = "CreatePledgeFragementCo";
    private String campaignId;
    RelativeLayout mainLayout;
    Button sendPledgeButton;
    private EditText commentEditText;
    private EditText pledgedAmountEditText;
    private String name;
    private String comment;
    private String pledgeAmount;
    private String phoneNumber;
    public static  Handler createPledgeHanlder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_create_pledge_view,container,false);
        mainLayout = view.findViewById(R.id.main_layout_create_pledge_id);
        commentEditText = view.findViewById(R.id.comments_edt_text_id);
        pledgedAmountEditText = view.findViewById(R.id.target_amount_edit_text_id);
        campaignId = getCampaignIdFromBundleId();
        HideKeyBoard hideKeyBoard = new HideKeyBoard(getActivity());
        hideKeyBoard.setupUI(mainLayout);
        sendPledgeButton = view.findViewById(R.id.send_pledge_button_id);
        setSendPledgeButton(sendPledgeButton);
        createPledgeHanlder = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                User newUser = (User) msg.obj;
                name = newUser.getDisplayName();
            }
        };
        FirebaseConnectors.getSingle(FirebaseConnectors.getCurrentUserUid().getUid(), createPledgeHanlder);
        return view;
    }
    private void setSendPledgeButton(Button sendPledgeButton){
        sendPledgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    phoneNumber = FirebaseConnectors.getCurrentUserUid().getPhoneNumber();
                    comment = commentEditText.getText().toString().trim();
                    pledgeAmount = pledgedAmountEditText.getText().toString().trim();
                    if(TextUtils.isEmpty(pledgeAmount)){
                        Toast.makeText(getContext(),"Please Add A Pledge Amount", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    FirebaseConnectors.sendPledgesToDb(campaignId, name, comment, pledgeAmount,phoneNumber);
                    // Removes the fragement transaction by destroying the activity that the fragement lives on. Because activity is destroyed no need for intent
                    getActivity().finish();
                }catch(Exception e){
                    Log.d(TAG,e.toString());
                }

            }
        });
    }
    private String getCampaignIdFromBundleId(){
        return getArguments().getString("id");
    }
}
