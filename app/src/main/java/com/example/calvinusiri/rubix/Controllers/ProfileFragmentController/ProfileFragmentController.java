package com.example.calvinusiri.rubix.Controllers.ProfileFragmentController;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.calvinusiri.rubix.Controllers.CreateCampaignActivityController.CreateCampaignMainActivity;
import com.example.calvinusiri.rubix.Controllers.LoginActivityController.LoginMainActivity;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.model.User;
import com.google.firebase.auth.FirebaseAuth;

public class ProfileFragmentController extends Fragment {
    private Button createNewCampaign;
    private Button logOutButton;
    private TextView nameCodeIconTextView;
    private TextView displayNameTextView;
    private TextView phoneNumberTextView;
    private String name;
    public static Handler profileFragmentControllerHandler;

    private FirebaseAuth auth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_view,container,false);
        createNewCampaign = (Button) view.findViewById(R.id.create_new_campaign);
        logOutButton = (Button) view.findViewById(R.id.logout_button_id);
        nameCodeIconTextView = view.findViewById(R.id.fragment_profile_view_text_view_name_code_id);
        displayNameTextView = view.findViewById(R.id.fragment_profile_view_display_name_text_view_id);
        phoneNumberTextView = view.findViewById(R.id.fragment_profile_view_phone_number_text_view_id);
        profileFragmentControllerHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                User newUser = (User) msg.obj;
                name = newUser.getDisplayName();
                setUpProfileViewInformation(name);
            }
        };
        FirebaseConnectors.getSingle(FirebaseConnectors.getCurrentUserUid().getUid(),profileFragmentControllerHandler);
        auth = FirebaseAuth.getInstance();

        createNewCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateCampaignMainActivity.class);
                startActivity(intent);
            }
        });
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                Intent intent = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
    private void setUpProfileViewInformation(String name){
        phoneNumberTextView.setText(FirebaseConnectors.getCurrentUserUid().getPhoneNumber());
        displayNameTextView.setText(name);
        Character firstLetter = name.charAt(0);
        int size = name.length()-1;
        String lastLetter = name.substring(size);
        nameCodeIconTextView.setText(firstLetter+lastLetter);
    }
}
