package com.example.calvinusiri.rubix.Controllers.ViewCampaignProfileActivityController;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.calvinusiri.rubix.Controllers.CreatePledgeFragmentController.CreatePledgeFragementController;
import com.example.calvinusiri.rubix.Controllers.LoginActivityController.LoginMainActivity;
import com.example.calvinusiri.rubix.Controllers.ViewPledgesFragementController.ViewPledgesFragmentController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.FragmentsSectionsStatePagerAdapter;
import com.google.firebase.auth.FirebaseAuth;

public class CampaignProfileViewController extends AppCompatActivity {
    private static final String TAG = "view_full_campaign_acti";
    private String campaignId;
    private Button pledgeButton;
    private Button shareButton;
    private ViewPager mViewPager;
    private ScrollView mainLayout;
    private LinearLayout pledgeAndShareButtonsLayout;
    private Button viewPledgesCommentsButon;
    private TextView campaignTitle;
    private TextView campaignDescrition;
    private TextView fundedTextView;
    private TextView targetAmountTextView;
    private TextView organizerPhoneNumberTextView;
    private TextView organizerCampaignNameTextView;
    private FirebaseAuth auth;
    private Context mContext = CampaignProfileViewController.this;

    @Override
    protected void onStart() {
        super.onStart();
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() == null){
            performSeque(mContext,LoginMainActivity.class);
        }
    }
    // Segue to signup activity
    public void performSeque(Context context, Class view){
        Intent intent = new Intent(context, view);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_campaign_profile);
        pledgeButton = findViewById(R.id.pledge_button_id);
        mainLayout = findViewById(R.id.scroll_view_main_layout_id);
        pledgeAndShareButtonsLayout = findViewById(R.id.donate_share_button_layout);
        viewPledgesCommentsButon = findViewById(R.id.view_pledges_id);
        shareButton = findViewById(R.id.share_button_id);
        campaignTitle = findViewById(R.id.text_view_title);
        campaignDescrition = findViewById(R.id.text_view_description);
        fundedTextView = findViewById(R.id.text_view_amount_funded_id);
        targetAmountTextView = findViewById(R.id.text_view_target_id);
        organizerPhoneNumberTextView = findViewById(R.id.text_view_display_phone_number);
        organizerCampaignNameTextView = findViewById(R.id.text_view_display_organizer_name);
        setUpPledgeCommentsFragmentButton(viewPledgesCommentsButon);
        setUpPledgeButtonOnClickShowPledgeFragement(pledgeButton);
        getIncomingIntent();
    }
    private void getIncomingIntent() {
        if (getIntent().hasExtra("campaign_id") && getIntent().hasExtra("downloaded_image_url") && getIntent().hasExtra("campaign_title")&& getIntent().hasExtra("campaign_organizer_id")
                && getIntent().hasExtra("campaign_description")) {
            campaignId = getIntent().getStringExtra("campaign_id");
            String imageUrl = getIntent().getStringExtra("downloaded_image_url");
            Uri imageUrlString = Uri.parse(imageUrl);
            String campaign_title = getIntent().getStringExtra("campaign_title");
            String campaign_organizer_id = getIntent().getStringExtra("campaign_organizer_id");
            String campaign_description = getIntent().getStringExtra("campaign_description");
            String campaign_orgnaizer_name = getIntent().getStringExtra("campaign_organzier_name");
            String campaign_orgnaizer_number = getIntent().getStringExtra("campaign_organizer_number");
            setProfileDetails(imageUrlString, campaign_title,campaign_organizer_id,campaign_description,campaign_orgnaizer_name,campaign_orgnaizer_number);
        }// Check to see if intent has extras to obtain first!!!
        if (getIntent().hasExtra("campaign_funded") && getIntent().hasExtra("campaign_target_amount") && getIntent().hasExtra("campaign_progress")&& getIntent().hasExtra("campaign_pledge_amount")) {
            String funded = getIntent().getStringExtra("campaign_funded");
            String targetAmount = getIntent().getStringExtra("campaign_target_amount");
            String progress = getIntent().getStringExtra("campaign_progress");
            String pledgeAmount = getIntent().getStringExtra("campaign_pledge_amount");
            setUpCampaignProgress(funded, targetAmount, progress,pledgeAmount);
        }
    }
    // Use these to set to widgets
    private void setProfileDetails(final Uri imageUrl, String campaign_title,String campaign_organizer_id,String campaignDescription,String campaignName,String campaignOrganizerNumber) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView campaignImage = findViewById(R.id.card_view_image_id);
                Glide.with(CampaignProfileViewController.this)
                        .load(imageUrl)
                        .asBitmap()
                        .into(campaignImage);
                ProgressBar progressBar = findViewById(R.id.campaign_image_progress_bar);
                progressBar.setVisibility(View.GONE);
            }
        });
        campaignTitle.setText(campaign_title);
        campaignDescrition.setText(campaignDescription);
        setUpProjectOrganizer(campaign_organizer_id);
        Button donate = findViewById(R.id.pledge_button_id);
        //Organizers should not be able to donate or pledge so remove the button, make it look better
        if (new String(FirebaseConnectors.getCurrentUserUid().getUid()).equals(campaign_organizer_id)) {
            donate.setVisibility(View.GONE);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 45;
            p.setMargins(0,0,0,10);
            p.setMarginEnd(10);
            shareButton.setLayoutParams(p);
            viewPledgesCommentsButon.setLayoutParams(p);

        }
        shareContentWith(shareButton, campaign_title);
    }
    // Set Up Views
    private void setUpProjectOrganizer(final String organizerId){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                FirebaseConnectors.getSingleUserFromDB(organizerId,organizerCampaignNameTextView,organizerPhoneNumberTextView);
            }
        });
    }
    private void setUpCampaignProgress(String funded, String targetAmount, String progress,String pledgeAmount) {
        fundedTextView.setText(funded);
        targetAmountTextView.setText(targetAmount);
        TextView pledgeAmountTextView = findViewById(R.id.text_view_pledged_id);
        pledgeAmountTextView.setText(pledgeAmount);
        ProgressBar progressBar = findViewById(R.id.feed_progress);
        progressBar.setProgress(Integer.parseInt(Integer.toString(Integer.parseInt(progress))));
    }
    // Onclick listeners
    private void setUpPledgeButtonOnClickShowPledgeFragement(Button pledgeButton){
        pledgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentStatePagerAdapter adapter = new FragmentsSectionsStatePagerAdapter(getSupportFragmentManager());
                Fragment createPledgeFragement = new CreatePledgeFragementController();
                genericPlegesFragmentLauncherAndCampaignIdSender(createPledgeFragement,adapter);
            }
        });
    }
    private void setUpPledgeCommentsFragmentButton(final Button viewPledges){
        viewPledges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentStatePagerAdapter adapter = new FragmentsSectionsStatePagerAdapter(getSupportFragmentManager());
                Fragment viewPledgesFragment = new ViewPledgesFragmentController();
                genericPlegesFragmentLauncherAndCampaignIdSender(viewPledgesFragment,adapter);
            }
        });
    }
    // Util Function
    private void genericPlegesFragmentLauncherAndCampaignIdSender(Fragment fragment, FragmentStatePagerAdapter adapter){
        Bundle args = new Bundle();
        args.putString("id",campaignId);
        fragment.setArguments(args);
        ((FragmentsSectionsStatePagerAdapter) adapter).addFragment(fragment);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(adapter);
        mainLayout.setVisibility(View.GONE);
        pledgeAndShareButtonsLayout.setVisibility(View.GONE);
    }

    private void shareContentWith(final Button shareButton, final String title){
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    launchIntentMessageSystemPicker(title);
                }catch (Exception e){
                    Log.d(TAG,e.toString());
                    Toast.makeText(CampaignProfileViewController.this,"How embarassing, please try again later",Toast.LENGTH_SHORT).show();
                    launchIntentMessageSystemPicker(title);
                }
            }
        });
    }
    private void launchIntentMessageSystemPicker(String title){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        String message = "Hi there,\nYou have been invited to participate in my fundraising campaign " +
                "called\n"+title+". \nGet started by downloading the rubi app below.\n\n"+"www.google.com"+"\n\n Team Rubi";
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(sendIntent);
    }
}