package com.example.calvinusiri.rubix.Controllers.FeedActivityController;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.calvinusiri.rubix.Controllers.FeedFragmentController.FeedFragmentController;
import com.example.calvinusiri.rubix.Controllers.LoginActivityController.LoginMainActivity;
import com.example.calvinusiri.rubix.Controllers.ProfileFragmentController.ProfileFragmentController;
import com.example.calvinusiri.rubix.Controllers.SearchFragmentController.SearchFragmentController;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FragmentsSectionsPageAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.testfairy.TestFairy;

public class FeedMainActivity extends AppCompatActivity {
    private static final String TAG = "FeedMainActivity";
    private FirebaseAuth auth;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: CREATED");
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() == null){
            Log.d(TAG, "onStart: Logged IN");
            auth.signOut();// Just to be 100 percent certain
            performSeque(FeedMainActivity.this,LoginMainActivity.class);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_main);
        TestFairy.begin(this, "654dc40b3f00afa2ac93f6ce01578941c0dfb5a2");
        setUpViewPager();

    }
    // Adds tabs to view pager which displace them
    private void setUpViewPager(){
        // Passing in the view pager to the function
        FragmentsSectionsPageAdapter adapter = new FragmentsSectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new SearchFragmentController(),"");
        adapter.addFragment(new FeedFragmentController(),"");
        adapter.addFragment(new ProfileFragmentController(), "");
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        //Set up the Viewpager with the section adapter, This is from the universal view pager
        mViewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);// TabLayout id in layout_tops_tabs
        tabLayout.setupWithViewPager(mViewPager);
        // Setting up the tab layout in activity_feed_main.xml
        //This connects the view pager to our tab layout
        //Setting icons to tabs, matches the indexes above
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_search);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_profile);
        mViewPager.setCurrentItem(1);// Makes the home tab the launching start tab
    }
    // Segue to signup activity
    public void performSeque(Context context, Class view){
        Intent intent = new Intent(context, view);
        startActivity(intent);
        finish();
    }
}
