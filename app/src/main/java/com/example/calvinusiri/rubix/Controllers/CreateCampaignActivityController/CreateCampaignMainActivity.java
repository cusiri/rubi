package com.example.calvinusiri.rubix.Controllers.CreateCampaignActivityController;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calvinusiri.rubix.Controllers.FeedActivityController.FeedMainActivity;
import com.example.calvinusiri.rubix.Controllers.LoginActivityController.LoginMainActivity;
import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.HideKeyBoard;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.util.UUID;

public class CreateCampaignMainActivity extends AppCompatActivity {
    private static final String TAG = "CreateCampaignMainActivity";
    private Button launchCampaignButton;
    private EditText titleEditText;
    private EditText descriptionEditText;
    private EditText targetAmountEditText;
    private TextView titleTextView;
    private ImageView campaignImage;
    private Uri filePath;
    private RelativeLayout mainLayout;
    public ProgressDialog imageProgressDialog;
    private FirebaseAuth auth;
    private Context mContext = CreateCampaignMainActivity.this;

    private int PICK_IMAGE_REQUEST = 71;
    @Override
    protected void onStart() {
        super.onStart();
        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() == null){
            performSeque(mContext,LoginMainActivity.class);
        }
    }
    // Segue to signup activity
    public void performSeque(Context context, Class view){
        Intent intent = new Intent(context, view);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_campaign_main);
        //Local instances
        launchCampaignButton = findViewById(R.id.begin_up_button_id);
        titleEditText = findViewById(R.id.title_edit_text_id);
        titleTextView = findViewById(R.id.title_text_view_id);
        descriptionEditText = findViewById(R.id.description_edt_text_id);
        targetAmountEditText = findViewById(R.id.target_amount_edit_text_id);
        campaignImage = findViewById(R.id.profile_image);
        mainLayout = findViewById(R.id.body_id);
        imageProgressDialog = new ProgressDialog(this);
        HideKeyBoard hideKeyBoard = new HideKeyBoard(CreateCampaignMainActivity.this);
        hideKeyBoard.setupUI(mainLayout);
        //Firebase instances
        realtimeTextViewUpdater();
        setCampaignImageClickedListenner();
        sendCampaignDataToDatabase(launchCampaignButton);
    }

    // Seque To FeedMainActivity
    public void segueToFeedMainActivity(View view){
        Intent intent = new Intent(this, FeedMainActivity.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        finish();
    }
    public void setCampaignImageClickedListenner(){
        campaignImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImageFromPhoneToSetAsCampaignImage();
            }
        });
    }

    public void realtimeTextViewUpdater(){

        titleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String value = editable.toString();
                titleTextView.setText(value);
            }
        });
    }

    // Send Campaign Data To Database
    public void sendCampaignDataToDatabase(Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = titleEditText.getText().toString().trim();
                String description = descriptionEditText.getText().toString().trim();
                String targetAmount = targetAmountEditText.getText().toString().trim();
                String phoneNumber = FirebaseConnectors.getCurrentUserUid().getPhoneNumber();
                UUID imageUrl = UUID.randomUUID();
                if(TextUtils.isEmpty(title)){
                    Toast.makeText(getApplicationContext(),getString(R.string.empty_title),Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(phoneNumber)){
                    Toast.makeText(getApplicationContext(),getString(R.string.empty_phone_number),Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(description)){
                    Toast.makeText(getApplicationContext(),getString(R.string.empty_description),Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(targetAmount)){
                    Toast.makeText(getApplicationContext(),getString(R.string.empty_target_amount),Toast.LENGTH_SHORT).show();
                    return;
                }
                FirebaseConnectors.launchCampaignToDatabase(getString(R.string.firebase_campaign_child),title,description,targetAmount,imageUrl,phoneNumber);
                uploadImageToDatabase(imageUrl);
            }
        });
    }
    //Choosing Image From Phone
    public void chooseImageFromPhoneToSetAsCampaignImage() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, PICK_IMAGE_REQUEST);
    }
    // On Complete it sets the image to the image view
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    filePath = data.getData();
                    try {
                        final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        campaignImage.post(new Runnable() {
                            @Override
                            public void run() {
                                campaignImage.setImageBitmap(bitmap);
                            }
                        });

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
    private void uploadImageToDatabase(UUID imageID) {
        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.variable_uploading));
            progressDialog.show();
            FirebaseConnectors.launchCampaignImageToStorge(getString(R.string.variable_images_directory_child)+imageID.toString(),
                    filePath,CreateCampaignMainActivity.this,getString(R.string.variable_uploaded),getString(R.string.variable_failed),progressDialog);
        }
    }
}
