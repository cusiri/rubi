package com.example.calvinusiri.rubix.Controllers.SearchFragmentController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.calvinusiri.rubix.R;
import com.example.calvinusiri.rubix.Utils.FirebaseConnectors;
import com.example.calvinusiri.rubix.Utils.HideKeyBoard;
import com.example.calvinusiri.rubix.model.Campaign;

import java.util.ArrayList;

public class SearchFragmentController extends Fragment{
    private ArrayList<Campaign> campaigns;
    private RelativeLayout mainLayout;
    private ImageView searchButton;
    private EditText searchBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_view,container,false);
        final RecyclerView recyclerView = view.findViewById(R.id.reusable_recycler_view);
        mainLayout = view.findViewById(R.id.fragment_search_main_layout_id);
        searchBar = view.findViewById(R.id.fragment_search_view_search_bar_id);
        searchButton = view.findViewById(R.id.fragment_search_view_search_button_id);
        HideKeyBoard hideKeyBoard = new HideKeyBoard(getActivity());
        hideKeyBoard.setupUI(mainLayout);
        campaigns = new ArrayList<>();
        final SearchFragmentRecyclerViewAdapter adapter = new SearchFragmentRecyclerViewAdapter(campaigns,getContext());//This is our default constructor that we made in our adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        setUpSearchButton(searchButton,searchBar,adapter,campaigns);
        return view;
    }
    private void setUpSearchButton(ImageView searchButton, final EditText text, final SearchFragmentRecyclerViewAdapter adapter, final ArrayList<Campaign> arrayList){
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayList.clear();
                Log.d("SearchBarFragement", "onClick: Search Button");
                String textQuery = text.getText().toString().trim();
                if(!TextUtils.isEmpty(textQuery)){
                    FirebaseConnectors.queryDbForCampaigns(textQuery,adapter, arrayList);
                }
            }
        });
    }
}
