package com.example.calvinusiri.rubix.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.calvinusiri.rubix.Controllers.FeedFragmentController.FeedFragmentRecyclerViewAdapter;
import com.example.calvinusiri.rubix.Controllers.SearchFragmentController.SearchFragmentRecyclerViewAdapter;
import com.example.calvinusiri.rubix.Controllers.ViewPledgesFragementController.PledgeFragmentRecyclerViewAdapter;
import com.example.calvinusiri.rubix.model.Campaign;
import com.example.calvinusiri.rubix.model.Pledges;
import com.example.calvinusiri.rubix.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.UUID;

public class FirebaseConnectors {
    private static final String TAG = "FirebaseConnectors";
    static private FirebaseStorage storage;
    static private StorageReference storageReference;
    static private DatabaseReference mDatabase;
    static private FirebaseAuth auth;
    static private String organizerId;
    //Get current user uid
    static public FirebaseUser getCurrentUserUid(){
        auth = FirebaseAuth.getInstance();
        return auth.getCurrentUser();
    }
    //Get from database
    // Must refractor all of this, breaking MVC
    static public void initGetImageUriCampaign(final Campaign imageCampaignUrl, final ImageView holder, final Context mContext, final ProgressBar progressBar){
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference().child("images/"+imageCampaignUrl.getImageCampaignUrl());
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // This is where we get the images, really this should communitcate with a handler and the glide method should be in the views
                // But I will do that later, this works at the moment
                imageCampaignUrl.setDownloadedCampaignUrl(uri);
                progressBar.setVisibility(View.GONE);
                Glide.with(mContext)
                        .load(uri)
                        .asBitmap()
                        .into(holder);
            }
        });
    }

    static public void initGetRawtextData(final ArrayList<Campaign> campaignArray, final FeedFragmentRecyclerViewAdapter adapter){
        FirebaseDatabase.getInstance().getReference().child("campaigns").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                campaignArray.clear();
                for(DataSnapshot s:dataSnapshot.getChildren()){
                    final Campaign campaign = s.getValue(Campaign.class);
                    campaign.setCampaignId(s.getKey());
                    campaignArray.add(campaign);
                    adapter.notifyDataSetChanged(); // Must be updated using DiffUtil Instead For efficiency
                };
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    static public void getSingleUserFromDB(final String organizerId, final TextView name, final TextView number){
        FirebaseDatabase.getInstance().getReference().child("users").child(organizerId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class); // Gets a single user who is the organizer of the campaign
                name.setText(user.getDisplayName());
                number.setText(user.getPhoneNumber());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    static public void getSingle(final String uid, final Handler mHandler){
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class); // Gets a single user who is the organizer of the campaign
                Message completeMessage = mHandler.obtainMessage(0,user);
                completeMessage.sendToTarget();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    static public void getCampaignPledgesFromDB(final ArrayList<Pledges> pledgesArray,String campaignId,final PledgeFragmentRecyclerViewAdapter adapter){
        FirebaseDatabase.getInstance().getReference().child("pledges").child(campaignId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pledgesArray.clear();
                for(DataSnapshot s:dataSnapshot.getChildren()){
                    final Pledges pledges = s.getValue(Pledges.class);
                    pledges.setId(s.getKey());
                    pledgesArray.add(pledges);
                    adapter.notifyDataSetChanged(); // Change to diffUtil later on
                };
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //Queries
    public static void queryDbForCampaigns(String query, final SearchFragmentRecyclerViewAdapter adapter, final ArrayList<Campaign> tmpArrayList){
        Query searchQuery = FirebaseDatabase.getInstance().getReference().child("campaigns").orderByChild("title").startAt(query).endAt(query+"\uf8ff");
        searchQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Campaign campaign = dataSnapshot.getValue(Campaign.class);
                System.out.println(dataSnapshot);
                tmpArrayList.add(campaign);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    //Send To Database, need to add completion blocks for progress bars
    public static void launchUserInformationToDatabase(String child,String displayName,String phoneNumber,String uid){
        mDatabase = FirebaseDatabase.getInstance().getReference();// Database reference
        DatabaseReference userRef = mDatabase.child(child).child(uid);// I am doing this because I do not want there to be duplicates in the DB
        userRef.setValue(new User(phoneNumber,displayName));
    }
    public static void sendPledgesToDb(String campaignId, String name, String comment, String pledgeAmount, String phoneNumber){
        mDatabase = FirebaseDatabase.getInstance().getReference();// Database reference
        DatabaseReference userRefPledges = mDatabase.child("pledges").child(campaignId).push();
        userRefPledges.setValue(new Pledges(name,comment,pledgeAmount,phoneNumber,"0"));
        updateTotalPledgeAmountInCampaign(pledgeAmount,campaignId);
    }
    public static void launchCampaignToDatabase(String databaseChild, String title, String description, String targetAmount,UUID imageUrl,String phoneNumber){
        mDatabase = FirebaseDatabase.getInstance().getReference();// Database reference
        organizerId = getCurrentUserUid().getUid();
        DatabaseReference userRef = mDatabase.child(databaseChild).push();
        userRef.setValue(new Campaign(organizerId,title,description,targetAmount,"0","0",imageUrl.toString(),phoneNumber));
    }
    public static void launchCampaignImageToStorge(String databaseChild, Uri filePath, final Context context, final String variableUploaded, final String variableFailed, final ProgressDialog progressDialog){
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference ref = storageReference.child(databaseChild);
        ref.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        Toast.makeText(context, variableUploaded, Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(context, variableFailed+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage(variableUploaded+(int)progress+"%");
                    }
                });
    }
    // Update Variable Methods
    private static void updateTotalPledgeAmountInCampaign(final String pledgeAmountFire, String campaignId){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("campaigns").child(campaignId);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Campaign campaign = dataSnapshot.getValue(Campaign.class);
                Float value = (Float) Float.valueOf(campaign.getTotalPledged());
                value = value + Float.valueOf(pledgeAmountFire);
                campaign.setTotalPledged(value.toString());
                dataSnapshot.getRef().setValue(campaign);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public static void updateTotalAmountFundedInCampaign(final String paid, String campaignId){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("campaigns").child(campaignId);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Campaign campaign = dataSnapshot.getValue(Campaign.class);
                Float value = (Float) Float.valueOf(campaign.getAmountFunded());
                value = value + Float.valueOf(paid);
                campaign.setAmountFunded(value.toString());
                dataSnapshot.getRef().setValue(campaign);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public static void updateAmountPaidFromAnIndividualPledge(final String paid,final String campaignId,String pledgeId){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("pledges").child(campaignId).child(pledgeId);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Pledges pledges = dataSnapshot.getValue(Pledges.class);
                Float value = (Float) Float.valueOf(pledges.getPaid());
                value = value + Float.valueOf(paid);
                pledges.setPaid(value.toString());
                dataSnapshot.getRef().setValue(pledges);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public static void deletePlegeFromDB(String campaignId, String pledgeId, final Context mContext, final Activity activity){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("pledges").child(campaignId).child(pledgeId);
        mDatabase.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                // These should be handled with handlers
                Toast.makeText(mContext,"Pledge Deleted",Toast.LENGTH_SHORT);
                activity.finish();
            }
        });
    }
}
